
const details = (arr,idNum) => {
  if(!arr || !idNum || !arr.length || idNum > arr.length || !Array.isArray(arr)){
    console.log('not a valid input')
    return
  }
  let carYear = arr[idNum-1]['car_year'];
  let carMake = arr[idNum-1]['car_make'];
  let carModel = arr[idNum-1]['car_model'];
  console.log(`Car ${idNum} is a ${carYear} ${carMake} ${carModel}`);
}
module.exports = details
