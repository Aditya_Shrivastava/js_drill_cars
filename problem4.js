// The accounting team needs all the years from every car on the lot.
// Execute a function that will return an array from the dealer data containing
// only the car years and log the result in the console as it was returned.
const years = (arr) => {
  if(!arr || !arr.length || !Array.isArray(arr)){
    return 'not a valid input'
  }
  let array = [];
  for(let i = 0; i < arr.length; i++){
    let current = arr[i]['car_year'];
    array.push(current);
  }
  return array;
};

module.exports = years
