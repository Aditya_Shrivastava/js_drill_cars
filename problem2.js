const details = (arr) => {
  if(!arr || !arr.length || !Array.isArray(arr)){
    console.log('not a valid input')
    return
  }
  let indexOfLast = arr.length-1;
  let carMake = arr[indexOfLast]['car_make'];
  let carModel = arr[indexOfLast]['car_model'];
  console.log(`Last car is a ${carMake} ${carModel}`);
}

module.exports = details
