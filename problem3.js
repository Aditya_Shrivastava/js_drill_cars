
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
const models = (arr) => {
  if(!arr || !arr.length || !Array.isArray(arr)){
    return 'not a valid input'
  }
  let newArray = [];
  for(let i = 0; i < arr.length; i++){
    let current = arr[i]['car_model'];
    newArray.push(current);
  }
  return newArray.sort();
};

module.exports = models
