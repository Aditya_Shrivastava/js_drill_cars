
// The car lot manager needs to find out how many cars are older than the year 2000. Using the
// array you just obtained from the previous problem, find out how many cars were made before
// the year 2000 and return the array of older cars and log its length.

let previousArray = [
  2009, 2001, 2010, 1983, 1990, 1995,
  2009, 1987, 1996, 2000, 2004, 2004,
  1997, 1999, 2000, 2001, 1987, 1995,
  1994, 1985, 2003, 1997, 1992, 2003,
  2005, 2005, 2000, 2005, 1993, 2010,
  1964, 1999, 2011, 1991, 2000, 2003,
  1997, 1992, 1998, 2012, 1965, 1996,
  2009, 2012, 2008, 1995, 2007, 2008,
  1996, 1999
];

const olderCars = (arr,prevArr) => {
  if(!arr || !arr.length || !Array.isArray(arr)){
    return 'not a valid input'
  }
  else if(!prevArr || !prevArr.length || !Array.isArray(prevArr)){
    return 'not a valid input'
  }
  let newArray = [];
  for(let i = 0; i < arr.length; i++){
    if(prevArr[i]<2000){
      newArray.push(arr[i]);
    }
  }
  return newArray;
};
module.exports = olderCars
