// A buyer is interested in seeing only BMW and Audi cars within the inventory.
// Execute a function and return an array that only contains BMW and Audi cars.
// Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const BMWAndAudi = (arr) => {
  if(!arr || !arr.length || !Array.isArray(arr)){
    return 'not a valid input'
  }
  let newArray = [];
  for(let i = 0; i < arr.length; i++){
    if(arr[i]['car_make'] === 'Audi' || arr[i]['car_make'] === 'BMW'){
      newArray.push(arr[i]);
    }
  }
  return newArray;
}
module.exports = BMWAndAudi
